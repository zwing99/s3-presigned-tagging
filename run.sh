#! /bin/bash -e

echo "Uploading lorem ipsum using presigned"
echo "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." >> tempfile.txt
read url presignedobj <<< $(./boto.py presigned)
curl --upload-file tempfile.txt $url
rm tempfile.txt


echo "Uploading lorem ipsum using boto3 put"
boto3obj=$(./boto.py put)


echo "tags on presigned"
aws s3api get-object-tagging --bucket zacoler.granappdevelopment.us-east-1.granular.ag --key $presignedobj
echo "tags on boto3 put"
aws s3api get-object-tagging --bucket zacoler.granappdevelopment.us-east-1.granular.ag --key $boto3obj
