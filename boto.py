#! /usr/bin/env python3
import boto3
import sys
import os
from uuid import uuid4

BUCKET_NAME = "zacoler.granappdevelopment.us-east-1.granular.ag"


def _get_uuid4():
    return str(uuid4())


def _get_s3_client():
    return boto3.client("s3")


def write_data_to_s3(data, tags):
    client = _get_s3_client()
    tagging = "&".join([f"{k}={v}" for k, v in tags.items()])
    key = f'boto3-{_get_uuid4()}.zip'
    resp = client.put_object(
        Bucket=BUCKET_NAME,
        Key=key,
        ServerSideEncryption="AES256",
        Tagging=tagging,
        Body=data,
    )
    return key


def get_presigned_post_url(tags, timeout=3600):
    client = _get_s3_client()
    tagging = "&".join([f"{k}={v}" for k, v in tags.items()])
    key = f'presigned-{_get_uuid4()}.zip'
    resp = client.generate_presigned_url(
        ClientMethod='put_object',
        Params=dict(
            Bucket=BUCKET_NAME,
            Key=key,
            ServerSideEncryption="AES256",
            Tagging=tagging,
        ),
        ExpiresIn=timeout,
    )

    return f'{resp} {key}'


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('not correct number of args, should be 2')
        sys.exit(1)
    elif sys.argv[1] == 'presigned':
        print(get_presigned_post_url(
            tags=dict(
                really='cool',
                awesome='sauce',
            )
        ))
    elif sys.argv[1] == 'put':
        print(write_data_to_s3(
            data='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            tags=dict(
                really='cool',
                awesome='sauce',
            )
        ))
    else:
        print('not correct args, should be presigned or put')
        sys.exit(1)
        
